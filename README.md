# Headless WordPress Theme

This theme is used when WordPress is setup to only be accessed via the API (headless). It includes the two required files (style.css and index.php)

Also includes functions.php which has several functions that:

* Registers Two Menus
* Adds ACF Options support
* Forces the REST URL API to use site_url()
* Add Featured Image ability for posts/pages

<?php
// Adds Menu Support
function fr_register_menus() {
	register_nav_menus(
		array(
			'primary' => esc_html__('Primary'),
			'footer' => esc_html__('Footer')
		)
	);
}
add_action('after_setup_theme', 'fr_register_menus');

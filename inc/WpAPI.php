<?php

if (get_site_url() != get_home_url()) {
  // Set the WP API url to match the site url if home url is different
  function add_the_subdomain($url, $path, $blog_id, $scheme) {
    $url = trailingslashit( get_site_url( $blog_id, rest_get_url_prefix(), $scheme ) );
    $path = ltrim( $path, '/' );
    $url = $url . $path;
    return $url;
  }
  add_filter( 'rest_url', 'add_the_subdomain', 10, 4 );
}

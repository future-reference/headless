<?php
/*
	Redirects all traffic back to Site Location url
	if different from Wordpress Address url
	needs to be set in General Seting
	*/

if (get_site_url() != get_home_url()) {
	wp_redirect(get_site_url());
	exit;
}
